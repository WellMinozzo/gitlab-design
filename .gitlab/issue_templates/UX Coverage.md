## Intent

This issue describes how broader group and team members are facilitating a members of the UX Department to take meaningful time away from work. The intent is that the folks aren't required to "catch-up" before and after [taking a well-deserved vacation][time-off]. 

## :handshake: Responsibilities

List the priorities that the responsible backups need to cover. Backups can be a peer, your manager, or a peer/manager in a different department (e.g. product manager).

Responsibilities include reviewing merge requests, responding to UX related questions, providing feedback related to inflight development work, or responding to Slack inquiries.

| Priority | Responsibility | Link | Primary backup DRI | Secondary backup DRI |
| ----------------- | -------------------- | ------------------ | ----------- | --------- |
| `HIGH/MEDIUM/LOW` | `e.g. Reviewing merge requests` | `ADD LINK` | `@assignee` | `@assignee` | 

## :muscle: Coverage Tasks

Include specific issue links for tasks to be completed by your backups.

- [ ] `Item` - `Link` - `@assignee`

## 🚙 Parked tasks

List the larger responsibilities that aren't moving forward until I return, such as creating a prototype or planning/conducting research.

- [ ] `Item` - `Link`

## :book: References

Here are some references for how I and my group generally works:

- [Group Process Handbook Page](ADD LINK)
- [Group UX Vision Handbook Page](ADD LINK)
- [Key direction pages](ADD LINK)
- [Additional References](ADD LINK)

## :white_check_mark: Issue Tasks

### :o: Opening Tasks

- [ ] Assign to yourself
- [ ] Title the issue `UX Coverage for YOUR-NAME from XXXX-XX-XX until XXXX-XX-XX`
- [ ] Add an issue comment for your retrospective titled: `:recycle: Retrospective Thread`
- [ ] Add any relevant references including direction pages, ux handbook pages, etc
- [ ] Fill in the Responsibilities table with broad based responsibilities
- [ ] Fill in the specific Coverage Tasks with distinct items to complete and assignees
- [ ] Assign to anyone with a specific task or responsibility assigned to them
- [ ] Share this issue in your section, stage and group Slack channels
- [ ] Ensure your PTO Ninja auto-responder points team members to this issue

### :x: Closing Tasks

- [ ] Assign back to yourself and remove others
- [ ] Review any Retrospective items with your counterparts and manager

<!-- Do not remove the items below -->

/label ~"UX Coverage Issue" 
[time-off]: https://about.gitlab.com/handbook/engineering/ux/how-we-work/#time-off
